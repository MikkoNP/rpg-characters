﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }

        public double DamagePerSecond { get; protected set; }

        public string ClassName { get; set; }
        //extra...
        public string mainstat { get; set; }

        protected PrimaryAttributes BasePrimaryAttributes { get; set; }
        //TotalPrimary doesn't need to be set...
        protected PrimaryAttributes TotalPrimaryAttributes { get; set; }
        protected SecondaryAttributes SecondaryAttributes { get; set; }

        protected Dictionary<ItemSlot, Item> Equipment { get; set; } = new Dictionary<ItemSlot, Item>();

        protected abstract int MainStatValue { get; }

        protected abstract ArmorType MaxArmorType {get;}
        protected abstract ArmorType MinArmorType {get;}

        protected abstract WeaponType MaxWeaponType { get; }
        protected abstract WeaponType MinWeaponType { get; }

        //override in subclasses
        public abstract void LevelUp();


        /// <summary>
        /// Refreshes the total primary and secondary attributes
        /// of calling object
        /// </summary>
        protected void RefreshStats()
        {
            TotalPrimaryAttributes = new PrimaryAttributes();
            
            TotalPrimaryAttributes.Dexterity = BasePrimaryAttributes.Dexterity;
            TotalPrimaryAttributes.Strength = BasePrimaryAttributes.Strength;
            TotalPrimaryAttributes.Intelligence = BasePrimaryAttributes.Intelligence;
            TotalPrimaryAttributes.Vitality = BasePrimaryAttributes.Vitality;

            if (Equipment.ContainsKey(ItemSlot.Legs))
            {
                Armor legs = (Armor)Equipment[ItemSlot.Legs];
                TotalPrimaryAttributes.Strength += legs.ArmorAttributes.Strength;
                TotalPrimaryAttributes.Intelligence += legs.ArmorAttributes.Intelligence;
                TotalPrimaryAttributes.Dexterity += legs.ArmorAttributes.Dexterity;
                TotalPrimaryAttributes.Vitality  += legs.ArmorAttributes.Vitality;

            }
            if (Equipment.ContainsKey(ItemSlot.Body))
            {
                Armor body = (Armor)Equipment[ItemSlot.Body];
                TotalPrimaryAttributes.Strength += body.ArmorAttributes.Strength;
                TotalPrimaryAttributes.Intelligence += body.ArmorAttributes.Intelligence;
                TotalPrimaryAttributes.Dexterity += body.ArmorAttributes.Dexterity;
                TotalPrimaryAttributes.Vitality += body.ArmorAttributes.Vitality;

            }
            if (Equipment.ContainsKey(ItemSlot.Head))
            {
                Armor head = (Armor)Equipment[ItemSlot.Head];
                TotalPrimaryAttributes.Strength += head.ArmorAttributes.Strength;
                TotalPrimaryAttributes.Intelligence += head.ArmorAttributes.Intelligence;
                TotalPrimaryAttributes.Dexterity += head.ArmorAttributes.Dexterity;
                TotalPrimaryAttributes.Vitality += head.ArmorAttributes.Vitality;

            }

            SecondaryAttributes.Health = TotalPrimaryAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = TotalPrimaryAttributes.Intelligence;

           
            if (Equipment.ContainsKey(ItemSlot.Weapon)) 
            {
                Weapon weapon = (Weapon)Equipment[ItemSlot.Weapon];
                double atkSpeed = weapon.WeaponAttributes.AttackSpeed;
                double damage = weapon.WeaponAttributes.Damage;

                DamagePerSecond = (atkSpeed * damage) * (1.0 + ((double)MainStatValue / 100.0));
            } else
            {
                DamagePerSecond = 0;
            }
            
        }

        /// <summary>
        /// Checks if item is weapon or armor, casts it into the right form
        /// and calls the right function        
        /// </summary>
        /// <param name="item">The item that is being added to equipped items</param>
        public void Equip(Item item)
        {
            //check if armor or weapon
            if( item.ItemLvl > Level){
                throw new InvalidItemLevel();
            }
            if (item.Slot == ItemSlot.Weapon) {
                EquipWeapon((Weapon)item);
            } else 
            {
                EquipArmor((Armor)item);
            }
        }

        /// <summary>
        /// Checks if character can use this particular armor piece
        /// and equips it if possible
        /// </summary>
        /// <param name="item">The armor item that will be equiped if possible</param>
        /// <exception cref="InvalidArmorException">the armor piece is not usable</exception>
        private void EquipArmor(Armor item)
        {

            // check type
            if ((int)item.ArmorType >= (int)MinArmorType 
            && (int)item.ArmorType <= (int)MaxArmorType)
            { 
                //we could unequip something...
                Equipment[item.Slot] = item;
                RefreshStats();

            } else
            {
                throw new InvalidArmorException();
            }
        }

        /// <summary>
        /// Checks if the weapon can be equiped and does that if possible
        /// </summary>
        /// <param name="item">the weapon item to be equiped if possible</param>
        /// <exception cref="InvalidWeaponException">unsuitable weapon</exception>
        private void EquipWeapon(Weapon item)
        {
            //the enums are sorted so that the mage only items are lowest (0 and 1)
            //currently works only for mage, but the "Min/MaxArmorType" concept can be implemented easily here
            if ((int)item.WeaponType >= (int)MinWeaponType
            && (int)item.WeaponType <= (int)MaxWeaponType)
            {
                Equipment[item.Slot] = item;
                RefreshStats();
            } else
            {
                throw new InvalidWeaponException("your character can't use this weapon");
            }
            
        }


        /// <summary>
        /// ToString override for printing all character information
        /// </summary>
        /// <returns>stringbuilder full of character information</returns>
        public override string ToString()
        {
            RefreshStats();
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Name: " + Name);
            stringBuilder.AppendLine("Lvl: " + Level + " " + ClassName);
            stringBuilder.AppendLine("\nSTR: " + TotalPrimaryAttributes.Strength);
            stringBuilder.AppendLine("DEX: " + TotalPrimaryAttributes.Dexterity);
            stringBuilder.AppendLine("INT: " + TotalPrimaryAttributes.Intelligence);
            stringBuilder.AppendLine("VIT: " + TotalPrimaryAttributes.Vitality);
            stringBuilder.AppendLine("\nHealth: " + SecondaryAttributes.Health);
            stringBuilder.AppendLine("Armor Rating: " + SecondaryAttributes.ArmorRating);
            stringBuilder.AppendLine("Elemental Resistance: " + SecondaryAttributes.ElementalResistance);
            stringBuilder.AppendLine("\nDPS: " + DamagePerSecond.ToString("#.##"));

            return stringBuilder.ToString();
        }
        
        /// <summary>
        /// Gets base stats (mainly for testing purposes)
        /// </summary>
        /// <returns>The base stats of a hero</returns>
        public PrimaryAttributes GetBaseStats()
        {
            PrimaryAttributes primarys = new PrimaryAttributes();

            primarys.Strength = BasePrimaryAttributes.Strength;
            primarys.Vitality = BasePrimaryAttributes.Vitality;
            primarys.Intelligence = BasePrimaryAttributes.Intelligence;
            primarys.Dexterity = BasePrimaryAttributes.Dexterity;

            return primarys;
        }
        /// <summary>
        /// gets total primary attributes for testing
        /// </summary>
        /// <returns>total primary attributes</returns>
        public PrimaryAttributes GetTotalStats()
        {
            PrimaryAttributes totals = new PrimaryAttributes();

            totals.Strength = TotalPrimaryAttributes.Strength;
            totals.Vitality = TotalPrimaryAttributes.Vitality;
            totals.Intelligence = TotalPrimaryAttributes.Intelligence;
            totals.Dexterity = TotalPrimaryAttributes.Dexterity;

            return totals;
        }
        /// <summary>
        /// fetches secondary stats for testing
        /// </summary>
        /// <returns>secondary stats of a hero</returns>
        public SecondaryAttributes GetSecondaryStats()
        {
            SecondaryAttributes secondaries = new SecondaryAttributes();

            secondaries.Health = SecondaryAttributes.Health;
            secondaries.ArmorRating = SecondaryAttributes.ArmorRating;
            secondaries.ElementalResistance = SecondaryAttributes.ElementalResistance;
            
            return secondaries;
        }
    }
}
