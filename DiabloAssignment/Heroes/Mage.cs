﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public class Mage : Hero
    {
        //starting stats 5, 1, 1, 8
      
        protected override int MainStatValue { get { return TotalPrimaryAttributes.Intelligence;} }

        protected override ArmorType MaxArmorType { get { return ArmorType.Cloth; } }
        protected override ArmorType MinArmorType { get { return ArmorType.Cloth; } }

        protected override WeaponType MaxWeaponType { get { return WeaponType.Wand; } }
        protected override WeaponType MinWeaponType { get { return WeaponType.Staff; } }

        public Mage()
        {
           
            
        }

        public Mage(string name, int lvl)
        {
            Name = name;
            Level = lvl;
            ClassName = "Mage";
            
            BasePrimaryAttributes = new PrimaryAttributes()
            {
                Vitality = 5,
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8

            };
            SecondaryAttributes = new SecondaryAttributes()
            {
                Health = BasePrimaryAttributes.Vitality * 10,
                ArmorRating = BasePrimaryAttributes.Strength + BasePrimaryAttributes.Dexterity,
                ElementalResistance = BasePrimaryAttributes.Intelligence,
                
            };
            
            DamagePerSecond = 0;

        }
        

        /// <summary>
        /// Overrides Hero Superclass LevelUp(). Mage specific level up stats inserted to Base primary attributes
        /// applied. LevelUp() calls refresh function to update derivative attributes
        /// </summary>
        public override void LevelUp()
        {
            Level = Level + 1;
            BasePrimaryAttributes.Dexterity++ ;
            BasePrimaryAttributes.Strength++;
            BasePrimaryAttributes.Intelligence += 5;
            BasePrimaryAttributes.Vitality += 3;

            RefreshStats();
            
        }

    }
}
