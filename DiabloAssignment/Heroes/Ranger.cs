﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public class Ranger : Hero
    {
        //rangerLvlGain = 2, 1, 5, 1;

        // 8, 1, 7, 1
        protected override int MainStatValue { get { return TotalPrimaryAttributes.Dexterity; } }

        protected override ArmorType MaxArmorType { get{ return ArmorType.Mail; } }
        protected override ArmorType MinArmorType { get { return ArmorType.Leather; } }

        protected override WeaponType MaxWeaponType { get { return WeaponType.Bow; } }
        protected override WeaponType MinWeaponType { get { return WeaponType.Bow; } }

        public Ranger(string name, int lvl)
        {
            this.Name = name;
            this.Level = lvl;
            ClassName = "Ranger";


            BasePrimaryAttributes = new PrimaryAttributes()
            {
                Vitality = 8,
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1

            };
            SecondaryAttributes = new SecondaryAttributes()
            {
                Health = BasePrimaryAttributes.Vitality * 10,
                ArmorRating = BasePrimaryAttributes.Strength + BasePrimaryAttributes.Dexterity,
                ElementalResistance = BasePrimaryAttributes.Intelligence,

            };

            DamagePerSecond = 0;

        }


        /// <summary>
        /// Overrides Hero Superclass LevelUp(). Ranger specific level up stats inserted to Base primary attributes
        /// applied. LevelUp() calls refresh function to update derivative attributes
        /// </summary>
        public override void LevelUp()
        {
            Level = Level + 1;
            BasePrimaryAttributes.Strength = BasePrimaryAttributes.Strength + 1;
            BasePrimaryAttributes.Intelligence = BasePrimaryAttributes.Intelligence + 1;
            BasePrimaryAttributes.Dexterity = BasePrimaryAttributes.Dexterity + 5;
            BasePrimaryAttributes.Vitality = BasePrimaryAttributes.Vitality + 2;


        }

        
        
    }
}
