﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public class Rogue : Hero
    {
        // rogueLvlGain = 3, 1, 4, 1;

        protected override int MainStatValue { get { return TotalPrimaryAttributes.Dexterity; } }

        protected override ArmorType MaxArmorType { get { return ArmorType.Mail; } }
        protected override ArmorType MinArmorType { get { return ArmorType.Leather; } }

        protected override WeaponType MaxWeaponType { get { return WeaponType.Sword; } }
        protected override WeaponType MinWeaponType { get { return WeaponType.Dagger; } }

        public Rogue(string name, int lvl)
        {
            this.Name = name;
            this.Level = lvl;
            ClassName = "Rogue";


            BasePrimaryAttributes = new PrimaryAttributes()
            {
                Vitality = 8,
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1

            };
            SecondaryAttributes = new SecondaryAttributes()
            {
                Health = BasePrimaryAttributes.Vitality * 10,
                ArmorRating = BasePrimaryAttributes.Strength + BasePrimaryAttributes.Dexterity,
                ElementalResistance = BasePrimaryAttributes.Intelligence,

            };

            DamagePerSecond = 0;

        }


        /// <summary>
        /// Overrides Hero Superclass LevelUp(). Rogue specific level up stats inserted to Base primary attributes
        /// applied. LevelUp() calls refresh function to update derivative attributes
        /// </summary>
        public override void LevelUp()
        {
            Level = Level + 1;
            BasePrimaryAttributes.Intelligence = BasePrimaryAttributes.Intelligence + 1;
            BasePrimaryAttributes.Strength = BasePrimaryAttributes.Strength + 1;
            BasePrimaryAttributes.Dexterity = BasePrimaryAttributes.Dexterity + 4;
            BasePrimaryAttributes.Vitality = BasePrimaryAttributes.Vitality + 3;


        }




    }


}
