﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public class Warrior : Hero
    {
        
        protected override int MainStatValue { get { return TotalPrimaryAttributes.Strength; } }

        protected override ArmorType MaxArmorType { get { return ArmorType.Plate; } }
        protected override ArmorType MinArmorType { get { return ArmorType.Mail; } }

        protected override WeaponType MaxWeaponType { get { return WeaponType.Hammer; } }
        protected override WeaponType MinWeaponType { get { return WeaponType.Sword; } }

        public Warrior(string name, int lvl)
        {
            Name = name;
            Level = lvl;
            ClassName = "Warrior";


            BasePrimaryAttributes = new PrimaryAttributes()
            {
                Vitality = 10,
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1

            };
            SecondaryAttributes = new SecondaryAttributes()
            {
                Health = BasePrimaryAttributes.Vitality * 10,
                ArmorRating = BasePrimaryAttributes.Strength + BasePrimaryAttributes.Dexterity,
                ElementalResistance = BasePrimaryAttributes.Intelligence,

            };

            DamagePerSecond = 0;

        }

        /// <summary>
        /// Overrides Hero Superclass LevelUp(). Warrior specific level up stats inserted to Base primary attributes
        /// applied. LevelUp() calls refresh function to update derivative attributes
        /// </summary>
        public override void LevelUp()
        {
            // warriorLvlGain = 5, 3, 2, 1;
            Level = Level + 1;
            BasePrimaryAttributes.Dexterity = BasePrimaryAttributes.Dexterity + 2;
            BasePrimaryAttributes.Strength = BasePrimaryAttributes.Strength + 3;
            BasePrimaryAttributes.Intelligence = BasePrimaryAttributes.Intelligence + 1;
            BasePrimaryAttributes.Vitality = BasePrimaryAttributes.Vitality + 5;


        }

       

    }
}
