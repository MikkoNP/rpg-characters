﻿using System;
using System.Runtime.Serialization;

namespace DiabloAssignment
{
    [Serializable]
    public class InvalidItemLevel : Exception
    {
        public InvalidItemLevel()
        {
        }

        public InvalidItemLevel(string message) : base(message)
        {
        }

        public InvalidItemLevel(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidItemLevel(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}