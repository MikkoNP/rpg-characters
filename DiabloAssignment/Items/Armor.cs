﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public class Armor : Item
    {
        private int armorValue { get; set; }

        public PrimaryAttributes ArmorAttributes { get; set; }
        public ArmorType ArmorType { get; set; }


        public Armor()
        {

        }

        public Armor(string name, int lvl, ItemSlot slot, ArmorType armortype, int strength, int dexterity, int intelligence, int vitality)
        {
            ItemName = name;
            ItemLvl = lvl;
            Slot = slot;
            ArmorType = armortype;
            ArmorAttributes = new PrimaryAttributes()
            {
                Strength = strength,
                Dexterity = dexterity,
                Intelligence = intelligence,
                Vitality = vitality,
            };

        }
        
    }
}
