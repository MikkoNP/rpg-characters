﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public class Weapon : Item
    {

        public WeaponAttributes WeaponAttributes { get; set; }
        public WeaponType WeaponType { get; set; }

        public Weapon() 
        {

        }
        public Weapon(string name, int level, WeaponType weapontype, int damage, int attackSpeed)
        {
            ItemName = name;
            ItemLvl = level;
            Slot = ItemSlot.Weapon;

            WeaponAttributes = new WeaponAttributes()
            {
                Damage = damage,
                AttackSpeed = attackSpeed,
            };
            WeaponType = weapontype; 

        }
    }
}
