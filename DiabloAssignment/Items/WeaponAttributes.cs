﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public class WeaponAttributes
    {
        public int Damage;
        public double AttackSpeed;
    }
}
