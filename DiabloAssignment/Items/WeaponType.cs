﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloAssignment
{
    public enum WeaponType
    {
        //i think we can put these in an order where we can get heroclass ok weapons
        // with a simple <x || >y
        Staff,
        Wand,
        Bow,
        Dagger,
        Sword,
        Axe,
        Hammer,
    }
}
