﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiabloAssignment
{
    class Program
    {

        static List<Hero> heroes = new List<Hero>();
        static List<Item> items = new List<Item>();

        /// <summary>
        /// main function. tools for choosing different program operations.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            
            

            //making some characters
            heroes.Add(new Mage("keke", 1 ));
            heroes.Add(new Ranger("matti", 1));
            heroes.Add(new Warrior("Gonan", 1));
            heroes.Add(new Rogue("Jon", 1));

            //test items
            Armor armor = new Armor("leggings of coding", 1, ItemSlot.Legs, ArmorType.Cloth, 1, 2, 3, 2);
            Weapon weapon = new Weapon("staff of staff management", 1, WeaponType.Staff, 4, 1);
            items.Add(armor);
            items.Add(weapon);

     
            //
            bool a = true;
            while (a)
            {
                Console.WriteLine("HERO INTERFACE");
                Console.WriteLine("\n1: Create Hero\n2: Display chosen Hero\n3: Level up Hero\n4: Equip item");
                string choice1 = Console.ReadLine();
                if (choice1 == "1")
                {
                    //create hero not currently done
                    HeroCreate();
                }
                else if (choice1 == "2")
                {
                    //choose hero and display stats
                    HeroDisplay();
                }
                else if (choice1 == "3")
                {
                    //lvl up chosen hero
                    ChooseLevelUp();
                } else if (choice1 == "4")
                {
                    //equip chosen item to chosen hero
                    EquipSomething();
                }
                else
                {
                    //any other input ends program
                    a = false;
                }
                
                
            }
        }

        /// <summary>
        /// allows user to choose hero from list and item from list
        /// calls equip function
        /// </summary>
        static void EquipSomething()
        {
            Console.WriteLine("who do you want to equip to?");

            for (int i = 0; i < heroes.Count; i++)
            {
                Console.WriteLine(i + 1 + ": " + heroes[i].Name + " | lvl " + heroes[i].Level + " " + heroes[i].ClassName);

            }
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice)
            || choice < 1 || choice > heroes.Count)
            {
                Console.WriteLine("invalid input");
            }
            choice--;

            Console.WriteLine("what item to equip?");
            for (int i = 0; i < items.Count; i++)
            {
                Console.WriteLine(i + 1 + ": " + items[i].ItemName + " | lvl " + items[i].ItemLvl );

            }
            int choice2;

            while (!int.TryParse(Console.ReadLine(), out choice2)
            || choice2 < 1 || choice2 > items.Count)
            {
                Console.WriteLine("invalid input");
            }
            choice2--;

            //chosen hero, equip chosen item
            heroes[choice].Equip(items[choice2]);
        }


        /// <summary>
        /// allows user to choose a hero and calls a Level up function for it
        /// </summary>
        static void ChooseLevelUp()
        {
            Console.WriteLine("who do you want to level up?");

            for (int i = 0; i < heroes.Count; i++)
            {
                Console.WriteLine(i + 1 + ": " + heroes[i].Name + " | lvl " + heroes[i].Level + " " + heroes[i].ClassName);

            }
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice)
            || choice < 1 || choice > heroes.Count)
            {
                Console.WriteLine("invalid input");
            }
            choice--;

            heroes[choice].LevelUp();
        }

        /// <summary>
        /// To be implemented.
        /// allows user to create new character
        /// </summary>
        static void HeroCreate()
        {
            //ask the basics, add to the list
        }

        /// <summary>
        /// user can choose a hero and it's current stats will be shown in console
        /// </summary>
        static void HeroDisplay()
        {
            Console.WriteLine("who do you want to see?");
            
            for (int i = 0; i < heroes.Count; i++)
            {
                Console.WriteLine(i + 1 + ": " + heroes[i].Name + " | lvl " + heroes[i].Level + " " + heroes[i].ClassName);
                
            }
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice) 
            || choice < 1 || choice > heroes.Count)
            {
                Console.WriteLine("invalid input");
            }
            choice--;


            Console.WriteLine(heroes[choice].ToString());

        }

    }
}
