using System;
using Xunit;
using DiabloAssignment;

namespace DiabloAssignmentTests
{
    public class Herotests
    {
        [Fact]
        public void Hero_CreateRogue_ShouldCreateRogueWithCorrectAttributes()
        {
            Rogue testRogue = new Rogue("test", 1);

            //expected 8vit, 2str, 6dex, 1int
            int expectedVit = 8;
            int expectedStr = 2;
            int expectedDex = 6;
            int expectedInt = 1;

            PrimaryAttributes actualAttributes = testRogue.GetBaseStats();
            
            Assert.Equal(expectedVit, actualAttributes.Vitality);
            Assert.Equal(expectedStr, actualAttributes.Strength);
            Assert.Equal(expectedDex, actualAttributes.Dexterity);
            Assert.Equal(expectedInt, actualAttributes.Intelligence);
        }

        [Fact]
        public void Hero_LevelUpMage_ShouldReturnMageWithCorrectAttributes()
        {
            Mage testmage = new Mage("test", 1);

            int expectedVit = 8;
            int expectedStr = 2;
            int expectedDex = 2;
            int expectedInt = 13;

            testmage.LevelUp();

            PrimaryAttributes actualAttributes = testmage.GetBaseStats();

            Assert.Equal(expectedVit, actualAttributes.Vitality);
            Assert.Equal(expectedStr, actualAttributes.Strength);
            Assert.Equal(expectedDex, actualAttributes.Dexterity);
            Assert.Equal(expectedInt, actualAttributes.Intelligence);

        }
        [Fact]
        public void Hero_CreateLevelUpAndEquipItems_ShouldShowCorrectFullStats()
        {
            Ranger testRanger = new Ranger("test", 1);
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLvl = 1,
                Slot = ItemSlot.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };
            Armor testArmor = new Armor()
            {
                ItemName = "leather tunic",
                ItemLvl = 2,
                Slot = ItemSlot.Body,
                ArmorType = ArmorType.Leather,
                ArmorAttributes = new PrimaryAttributes() { Dexterity = 1, Strength = 1 }

            };
            testRanger.LevelUp();
            testRanger.Equip(testBow);
            testRanger.Equip(testArmor);

            int expectedVit = 10;
            int expectedStr = 3;
            int expectedDex = 13;
            int expectedInt = 2;
            int expectedHp = 100;
            int expectedArmRate = 16;
            int expectedEleRes = 2;
            double expectedDps = 10.848;

            PrimaryAttributes actualAttributes = testRanger.GetTotalStats();
            SecondaryAttributes actualSecondaries = testRanger.GetSecondaryStats();
            

            Assert.Equal(expectedVit, actualAttributes.Vitality);
            Assert.Equal(expectedStr, actualAttributes.Strength);
            Assert.Equal(expectedDex, actualAttributes.Dexterity);
            Assert.Equal(expectedInt, actualAttributes.Intelligence);
            Assert.Equal(expectedHp, actualSecondaries.Health);
            Assert.Equal(expectedEleRes, actualSecondaries.ElementalResistance);
            Assert.Equal(expectedArmRate, actualSecondaries.ArmorRating);
            Assert.Equal(expectedDps, testRanger.DamagePerSecond);

            

        }
        
    }
}
