using System;
using Xunit;
using DiabloAssignment;

namespace DiabloAssignmentTests
{
    public class Itemtests
    {

        [Fact]
        public void Item__CreateAndTryEquipWeaponToWarrior_ShouldSucceed()
        {
            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLvl = 1,
                Slot = ItemSlot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Warrior testWarrior = new Warrior("test", 1);
            testWarrior.Equip(testAxe);
           
        }
        [Fact]
        public void Item_CreateAndTryEquipWrongTypeWeaponToMage_ShoudThrowInvalidWeaponException()
        {
            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLvl = 1,
                Slot = ItemSlot.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };
            Mage testmage = new Mage("test", 1);

            Assert.Throws<InvalidWeaponException>(() => testmage.Equip(testBow));
        }
        [Fact]
        public void Item_CreateAndTryToEquipTooHighLevelWeapon_ShoudThrowInvalidItemLevelException()
        {
            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLvl = 2,
                Slot = ItemSlot.Body,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            Warrior testWarrior = new Warrior("test", 1);

            Assert.Throws<InvalidItemLevel>(() => testWarrior.Equip(testPlateBody));

        }
           
         
       
    }
}
